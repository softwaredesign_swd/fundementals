public class CashRegister {

    private int numberOfTwentiesInRegister;
    private int numberOfTensInRegister;
    private int numberOfFivesInRegister;
    private int numberOfOnesInRegister;
    private int numberOfQuartersInRegister;
    private int numberOfDimesInRegister;
    private int numberOfNickelsInRegister;
    private int numberOfPenniesInRegister;

    public CashRegister(int twenties, int tens, int fives, int ones,
                        int quarters, int dimes, int nickels, int pennies){
        numberOfTwentiesInRegister = twenties;
        numberOfTensInRegister = tens;
        numberOfFivesInRegister = fives;
        numberOfOnesInRegister = ones;
        numberOfQuartersInRegister = quarters;
        numberOfDimesInRegister = dimes;
        numberOfNickelsInRegister = nickels;
        numberOfPenniesInRegister = pennies;
    }

    public int getNumberOfTwentiesInRegister() {
        return numberOfTwentiesInRegister;
    }

    public void setNumberOfTwentiesInRegister(int numberOfTwentiesInRegister) {
        this.numberOfTwentiesInRegister = numberOfTwentiesInRegister;
    }

    public int getNumberOfTensInRegister() {
        return numberOfTensInRegister;
    }

    public void setNumberOfTensInRegister(int numberOfTensInRegister) {
        this.numberOfTensInRegister = numberOfTensInRegister;
    }

    public int getNumberOfFivesInRegister() {
        return numberOfFivesInRegister;
    }

    public void setNumberOfFivesInRegister(int numberOfFivesInRegister) {
        this.numberOfFivesInRegister = numberOfFivesInRegister;
    }

    public int getNumberOfOnesInRegister() {
        return numberOfOnesInRegister;
    }

    public void setNumberOfOnesInRegister(int numberOfOnesInRegister) {
        this.numberOfOnesInRegister = numberOfOnesInRegister;
    }

    public int getNumberOfQuartersInRegister() {
        return numberOfQuartersInRegister;
    }

    public void setNumberOfQuartersInRegister(int numberOfQuartersInRegister) {
        this.numberOfQuartersInRegister = numberOfQuartersInRegister;
    }

    public int getNumberOfDimesInRegister() {
        return numberOfDimesInRegister;
    }

    public void setNumberOfDimesInRegister(int numberOfDimesInRegister) {
        this.numberOfDimesInRegister = numberOfDimesInRegister;
    }

    public int getNumberOfNickelsInRegister() {
        return numberOfNickelsInRegister;
    }

    public void setNumberOfNickelsInRegister(int numberOfNickelsInRegister) {
        this.numberOfNickelsInRegister = numberOfNickelsInRegister;
    }

    public int getNumberOfPenniesInRegister() {
        return numberOfPenniesInRegister;
    }

    public void setNumberOfPenniesInRegister(int numberOfPenniesInRegister) {
        this.numberOfPenniesInRegister = numberOfPenniesInRegister;
    }
}
