import java.util.Scanner;
import java.util.Random;

public class ChangeComputationDriver {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        Random randomNumber = new Random();

        System.out.println("Enter price of item and amount customer paid separated by spaces: ");

        double priceOfItem = input.nextDouble();
        double amountCustomerPaid = input.nextDouble();

        int numberTwentyDollarBills = randomNumber.nextInt((15 - 0) + 1) + 0;
        int numberTenDollarBills = randomNumber.nextInt((15 - 0) + 1) + 0;
        int numberFiveDollarBills = randomNumber.nextInt((15 - 0) + 1) + 0;
        int numberOneDollarBills = randomNumber.nextInt((15 - 0) + 1) + 0;
        int numberQuarters = randomNumber.nextInt((15 - 0) + 1) + 0;
        int numberDimes = randomNumber.nextInt((15 - 0) + 1) + 0;
        int numberNickels = randomNumber.nextInt((15 - 0) + 1) + 0;
        int numberPennies = randomNumber.nextInt((15 - 0) + 1) + 0;

        CashRegister billsInCashRegister = new CashRegister(numberTwentyDollarBills, numberTenDollarBills,
                                                        numberFiveDollarBills, numberOneDollarBills,
                                                        numberQuarters, numberDimes, numberNickels, numberPennies);

        Change customerChange = new Change(priceOfItem, amountCustomerPaid, billsInCashRegister);
        System.out.print("Your Change is: $");
        System.out.println(customerChange.getChange()+ "\n");


        customerChange.billsAndCoinsToReturn(customerChange.getChange());
        System.out.println("You will receive the following bills: ");
        System.out.print("Twenties: ");
        System.out.println(customerChange.getDollars20());
        System.out.print("Tens: ");
        System.out.println(customerChange.getDollars10());
        System.out.print("Fives: ");
        System.out.println(customerChange.getDollars5());
        System.out.print("Ones: ");
        System.out.println(customerChange.getDollars1());
        System.out.print("Quarters: ");
        System.out.println(customerChange.getQuarters());
        System.out.print("Dimes: ");
        System.out.println(customerChange.getDimes());
        System.out.print("Nickels: ");
        System.out.println(customerChange.getNickels());
        System.out.print("Pennies: ");
        System.out.println(customerChange.getPennies());

    }
}
